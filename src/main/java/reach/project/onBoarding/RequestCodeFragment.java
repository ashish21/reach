package reach.project.onBoarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.lang.ref.WeakReference;

import reach.project.R;
import reach.project.core.ReachApplication;
import reach.project.core.StaticData;

/**
 * Created by Dexter on 30-06-2015.
 */
public class RequestCodeFragment extends Fragment {

    private static WeakReference<FragmentManager> reference;
    private static WeakReference<View> layoutToShow;

    public static RequestCodeFragment newInstance(String phoneNumber, FragmentManager manager, View view) {

        reference = new WeakReference<>(manager);
        layoutToShow = new WeakReference<>(view);
        final RequestCodeFragment requestCodeFragment = new RequestCodeFragment();
        final Bundle bundle = new Bundle();
        bundle.putString("phone_number", phoneNumber);
        requestCodeFragment.setArguments(bundle);
        return requestCodeFragment;
    }

    private void makeRequest(String email, Tracker tracker) {

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Invite Code Request")
                .setAction("User - " + getArguments().getString("phone_number"))
                .setLabel(email)
                .setValue(1)
                .build());
    }

    private final View.OnClickListener exitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final FragmentManager manager;
            final View toShow;
            if (reference == null || layoutToShow == null ||
                    (manager = reference.get()) == null || (toShow = layoutToShow.get()) == null) {

                final FragmentActivity activity = getActivity();
                if (activity != null) {
                    makeRequest("", ((ReachApplication) getActivity().getApplication()).getTracker());
                    activity.finish();
                }
                return;
            }

            //TODO why pop ?
            manager.popBackStack();
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toShow.setVisibility(View.VISIBLE);
                }
            }, 500);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.request_code_dialog, container, false);
        final EditText iCode = (EditText) rootView.findViewById(R.id.iCode);
        rootView.findViewById(R.id.exit).setOnClickListener(exitListener);
        iCode.requestFocus();

        rootView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final FragmentActivity activity = getActivity();

                if(activity == null || isRemoving() || activity.isFinishing())
                    return;

                if (TextUtils.isEmpty(iCode.getText()) || !Patterns.EMAIL_ADDRESS.matcher(iCode.getText()).matches()) {
                    Toast.makeText(activity, "Please enter a valid Email ID", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!StaticData.debugMode)
                    makeRequest(iCode.getText().toString(), ((ReachApplication) activity.getApplication()).getTracker());

                iCode.setEnabled(false);
                iCode.setTextColor(getResources().getColor(R.color.reach_color));
                rootView.findViewById(R.id.done).setVisibility(View.GONE);
                rootView.findViewById(R.id.text2).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.finish();
                    }
                });
            }
        });
        return rootView;
    }
}

